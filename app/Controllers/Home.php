<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		// $all_pekan = [[
		// 	'nama' => "Seremban",
		// 	'gambar' => 'https://images.unsplash.com/photo-1586407596876-b4f9d51a85f2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mzl8fHBlcmFrfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=400&q=60',
		// 	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
		// ],  [
		// 	'nama' => "Gemas",
		// 	'gambar' => 'https://images.unsplash.com/photo-1512310604669-443f26c35f52?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fHNleHklMjB3b21hbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=60',
		// 	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
		// ],  [
		// 	'nama' => "Jasin",
		// 	'gambar' => 'https://images.unsplash.com/photo-1495434350527-8f3d63bf008f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8aXBvaHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60',
		// 	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
		// ], [
		// 	'nama' => "Taiping",
		// 	'gambar' => 'https://images.unsplash.com/photo-1559755693-6e9abcf39789?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8aXBvaHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60',
		// 	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
		// ], [
		// 	'nama' => "Ipoh",
		// 	'gambar' => 'https://images.unsplash.com/photo-1622665645573-b0b5dea09d98?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzZ8fGxhbmdrYXdpfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60',
		// 	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
		// ], [
		// 	'nama' => "Alor Setar",
		// 	'gambar' => 'https://images.unsplash.com/photo-1502289331255-6145bace4657?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NTF8fGxhbmdrYXdpfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60',
		// 	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
		// ]

		// ];
		
		$db = db_connect();

		$result = $db->query('SELECT * FROM gambar ORDER BY nama asc');
		$all_pekan = $result->getResult();

		//dd($all_pekan);

		return view('homepage', ['all_pekan' => $all_pekan]);
	}

	public function hello()
	{
		echo "<h1>Hello...</h1>";
	}

	public function welcome()
	{
		echo "<h1>Welcome</h1>";
	}
}

  

//$all_pekan[] = $pekan;